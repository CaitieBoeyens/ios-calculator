//
//  HistoryViewController.swift
//  Calculator
//
//  Created by Student on 2019/10/24.
//  Copyright © 2019 openwindow. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    let identifier = "HistoryTableViewCell"
    var historyList: [Calculation] = []

    @IBOutlet weak var historyTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        historyTable.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func addCalculation(calc:Calculation){
        historyList += [calc]
        print(historyList)
    }
    

    @IBAction func closeModal(_ sender: UIButton) {
        self.dismiss(animated: true
            , completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = historyTable.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? HistoryTableViewCell {
            
            let history = historyList[indexPath.row]
            
            cell.nameLabel.text = history.name
            cell.calcLabel.text = history.calcString
            cell.answerLabel.text = "\(history.answer)"
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let history = historyList[indexPath.row]
        
        let alert = UIAlertController(title: "New calculation name", message: "Enter a new name for your calculation", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.text = history.name
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert!.textFields![0]
            guard let name = textField.text else {
                return
            }
            
            print("Text field: \(name)")
            
            if let cell = self.historyTable.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as? HistoryTableViewCell {
                history.name = name
                cell.nameLabel.text = history.name
            }
            self.historyTable.reloadData() 
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
