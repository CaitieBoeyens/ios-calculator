//
//  UIButton+Designable.swift
//  project
//
//  Created by Student on 2019/10/10.
//  Copyright © 2019 Caitie Boeyens. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableButton: UIButton {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var ShadowColor: CGColor{
        get {
            return layer.shadowColor ?? UIColor.black.cgColor
        }
        set {
            layer.shadowColor = newValue
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue / 10
        }
    }
    
    @IBInspectable
    var rounded: Bool = false {
        didSet {
            if rounded {
                layer.cornerRadius = layer.bounds.height / 2
            } else {
                layer.cornerRadius = 0
            }
        }
    }
    
}
