	//
//  Calculation.swift
//  Calculator
//
//  Created by Brave Mac on 2019/10/30.
//  Copyright © 2019 openwindow. All rights reserved.
//

import UIKit
    
    class Calculation {
        
        //MARK: Properties
        
        var name: String
        var calcString: String
        var answer: Double
        
        init(name: String, calcString: String, answer: Double) {
            self.name = name
            self.calcString = calcString
            self.answer = answer
        }
        
    }
