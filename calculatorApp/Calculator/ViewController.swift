//
//  ViewController.swift
//  Calculator
//
//  Created by Student on 2019/10/10.
//  Copyright © 2019 openwindow. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var memoryText: UILabel!
    @IBOutlet weak var resultText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    var resultString:String = "0"
    var firstValue = true
    var subTotal: Double?
    var memory:Double = 0
    var memoryString:String = "0"
    var historyString:String=""
    
    var historyList: [Calculation] = []
    
    enum operatorType {
        case add
        case subtract
        case multiply
        case divide
    }
    
    var lastOperator: operatorType = .add
    
    
    func updateResultLabel() {
        if resultText.text != nil {
            resultText.text = resultString
        }
    }
    
    @IBAction func onHistoryTap(_ sender: UIButton) {
        performSegue(withIdentifier: "toHistory", sender: historyList)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? HistoryViewController {
            if let calculations = sender as? [Calculation] {
                viewController.historyList = calculations
            }
        }
    }
    
    @IBAction func clearClicked(_ sender: UIButton) {
        resultString = "0"
        subTotal = nil
        firstValue = true
        lastOperator = .add
        historyString=""
        print(historyString)
        updateResultLabel()
    }
    
    @IBAction func numberClicked(_ sender: UIButton) {
        if let number: String = sender.titleLabel?.text {
            if firstValue {
                resultString = number
                firstValue = false
            } else {
                resultString += number
            }
            historyString += number
            print(historyString)
            updateResultLabel()
        }
    }
    
    func setOperator(op:String) {
        switch op {
            case "x" :
                lastOperator = .multiply
            case "÷" :
                lastOperator = .divide
            case "+" :
                lastOperator = .add
            case "-" :
                lastOperator = .subtract
            default:
                break
            }
    }
    
    func getResult(currentSubTotal:Double)->Double{
        var result: Double
        switch lastOperator {
            case .multiply :
                result = currentSubTotal * Double(resultString)!
            case .divide :
                result = currentSubTotal / Double(resultString)!
            case .add :
                result = currentSubTotal + Double(resultString)!
            case .subtract :
                result = currentSubTotal - Double(resultString)!
            
        }
        return result
    }
    
    @IBAction func operatorClicked(_ sender: UIButton) {
        if let operatorText: String = sender.titleLabel?.text {
            if let currentSubTotal = subTotal {
                let result = getResult(currentSubTotal: currentSubTotal)
                subTotal = result
            } else {
                subTotal = Double(resultString)!
            }
            if !firstValue {
                historyString += operatorText
                print(historyString)
            }
            
            setOperator(op: operatorText)
            firstValue = true
            resultString  = "\(subTotal!)"
            updateResultLabel()
        }
        
    }
    
    @IBAction func equalsClicked(_ sender: UIButton) {
        var result: Double?
        if let currentSubTotal = subTotal {
            result = getResult(currentSubTotal: currentSubTotal)
        }
        if let currentResult = result {
            resultString = "\(currentResult)"
            let calc = Calculation(name: "", calcString: historyString, answer: currentResult)
            historyList.append(calc)
        } else {
            resultString = "0"
        }
        subTotal=nil
        updateResultLabel()
        historyString=""
        
        print(historyString)
        firstValue = true
    }
    
    func clearMemory() {
        memory = 0
        memoryString = "0"
    }
    
    @IBAction func memoryClear(_ sender: UIButton) {
        clearMemory()
        updateMemoryLabel()
    }
    @IBAction func memoryRecall(_ sender: UIButton) {
        resultString = memoryString
        subTotal = nil
        firstValue = true
        clearMemory()
        updateResultLabel()
        updateMemoryLabel()
        firstValue = false
    }
    @IBAction func memoryOperatorClicked(_ sender: UIButton) {
        let result:Double
        if let currentSubTotal = subTotal {
            result = getResult(currentSubTotal: currentSubTotal)
        } else {
            result = Double(resultString)!
        }
        
        if let operatorText: String = sender.titleLabel?.text {
            switch operatorText {
            case "M+":
                memory += result
            case "M-":
                memory -= result
            default:
                break
            }
        memoryString = "\(memory)"
        resultString = "0"
        subTotal = nil
        firstValue = true
        updateResultLabel()
        updateMemoryLabel()
        }
    }
    
    func updateMemoryLabel() {
        if memoryText.text != nil {
            memoryText.text = memoryString
        }
    }
}

